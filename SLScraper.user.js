// ==UserScript==
// @name        SL Scraper
// @author      Chris Merkley
// @namespace   https://uk2group.com
// @description Scrape the details of a SL server
// @include     https://control.softlayer.com/devices/details/*
// @downloadURL https://bitbucket.org/thisismetroid/slscraper/raw/master/SLScraper.user.js
// @version     .3.5
// @grant       GM_xmlhttpRequest
// @run-at      document-idle
// ==/UserScript==

//++++++++++++++++++++++++++++THESE NEED TO BE SET TO YOUR SL USERNAME AND APIKey++++++++++++++++++++++++++++++
var username = "";
var APIKey = "";
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
var reg = new RegExp('\\d+');
var server = reg.exec(window.location.href)[0];
var modal;
var btn;

window.addEventListener ("load", Greasemonkey_main, false);

function Greasemonkey_main() {
    var resolved = document.getElementsByTagName("h5")[0];

    let checkMenu = setTimeout(function check() {
      if (typeof resolved !== "undefined") {
          createMenuLink();
          setBtn();
      }
      else {
          resolved = document.getElementsByTagName("h5")[0];
          console.log("Failed to get menu element (this is normal)");
          checkMenu = setTimeout(check, 250);// (*)
      }
    }, 250);
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};

function setModal() {
    // Get the modal
    modal = document.getElementById('myModal');
}

function setBtn() {
    // Get the button that opens the modal
    btn = document.getElementById("provLink");

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        if (modal === undefined) {
        	createModal();
          setModal();
          setBtnSub();
        }
        else {
          setBtnSub();
        }
    };
}

function setBtnSub() {
    modal.style.hidden = false;
    modal.style.display = "block";
}

function createMenuLink() {
	var button = document.createElement("button");
	button.innerHTML = "Provisioning Details";
	button.setAttribute("style", "float: right;");
	button.setAttribute("id", "provLink");
	document.getElementsByTagName('h5')[0].prepend(button);
}

function createModal() {
  //Initialize Modal
  var modalOut = document.createElement("div");
  var modalIn = document.createElement("div");
  var pre = document.createElement("pre");
  pre.setAttribute("id", "dontchangethis");
  pre.innerHTML = createMessage();
  modalOut.setAttribute("id", "myModal");
  modalOut.setAttribute("class", "modal");
  modalIn.setAttribute("class", "modal-content");
  modalIn.appendChild(pre);

  //Combine elements
  modalOut.appendChild(modalIn);
  document.body.appendChild(modalOut);
}

function createMessage() {
  var messageFinal = "";

  GM_xmlhttpRequest({
  	method: 'GET',
  	url: 'https://' + username + ':' + APIKey + '@api.softlayer.com/rest/v3/SoftLayer_Hardware_Server/' + server + '/getObject.json?objectMask=operatingSystem.passwords;remoteManagementAccounts.username;remoteManagementAccounts.password;datacenter',
    headers: {"Accept": "application/json"},
  	onload: function(response) {
        response = JSON.parse(response.response);
    	var serverIDReg = new RegExp('\\d+');
        console.log(response);
  		var serverID = serverIDReg.exec(response.hostname)[0];
    	var locationReg = new RegExp('[a-zA-Z]*\\s*?[a-zA-Z]*(?=\\s\\d)');
    	var location = locationReg.exec(response.datacenter.longName);
    	var OSReg = new RegExp('^[a-zA-Z\\.\\d]*\\s?[a-zA-Z]*?\\s?\\d+\\s?[Rr12]*');
    	var OS = OSReg.exec(response.operatingSystem.softwareLicense.softwareDescription.longDescription);
    	messageFinal = "Server Details:\n" +
      	"==========\n" +
      	"Server Name: " + response.hostname + ".hosted-by-100tb.com\n" +
      	"Main IP: " + response.primaryIpAddress + "\n" +
      	"User: root / " + response.operatingSystem.passwords[0].password + "\n" +
      	"Operating System: " + OS + "\n" +
      	"Location: " + location + "\n" +
      	"\n" +
      	"IPMI Information:\n" +
      	"==========\n" +
      	"IP Address: " + response.networkManagementIpAddress + "\n" +
      	"User: root / " + response.remoteManagementAccounts[0].password + "\n\n" +
      	"For pasting to spreadsheet (highlight cell(s) > data > split text into columns) \n==========\n" +
      	serverID.toString() + "," + response.primaryIpAddress + "," + response.operatingSystem.passwords[0].password + "," + response.networkManagementIpAddress + " / " + response.remoteManagementAccounts[0].password + "," + location + "\n\n" +
      	"For pasting to servers.lst(server check script)\n==========\n" +
      	serverID.toString() + " " + response.primaryIpAddress + " " + response.operatingSystem.passwords[0].password;
    	console.log("Message Created");
    	var pre = document.getElementById("dontchangethis");
    	pre.innerHTML = messageFinal;
    }
  });

	return messageFinal;
}


function addGlobalStyle(css) {
  var head, style;
  head = document.getElementsByTagName('head')[0];
  if (!head) { return; }
  style = document.createElement('style');
  style.type = 'text/css';
  style.innerHTML = css;
  head.appendChild(style);
}

addGlobalStyle(".modal {                                       \
  hidden: true;                                                \
  display: none; /* Hidden by default */                       \
  position: fixed; /* Stay in place */                         \
  z-index: 1; /* Sit on top */                                 \
  padding-top: 100px; /* Location of the box */                \
  margin: 0 auto;                                              \
  width: 100%; /* Full width */                                \
  height: 100%; /* Full height */                              \
  overflow: auto; /* Enable scroll if needed */                \
  overflow-x: auto;                                            \
  background-color: rgb(0,0,0); /* Fallback color */           \
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */    \
}                                                              \
");

addGlobalStyle("/* Modal Content */                            \
.modal-content {                                               \
  all: revert;                                                 \
  background-color: #fefefe;                                   \
  overflow-x: auto;                                            \
  margin: auto;                                                \
  padding: 20px;                                               \
  border: 1px solid #888;                                      \
  width: 50%;                                                  \
}                                                              \
");

addGlobalStyle("#dontchangethis {                              \
  all: revert;                                                 \
}                                                              \
");

